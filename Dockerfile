# syntax=docker/dockerfile:1.0.0-experimental
# The above line is required to turn on experimental BuildKit features.
# Dockerfile.private - Build Traefik and plugin from a private git repository.
# Loads SSH keys from the host `ssh-agent` to allow git clone.
FROM alpine:3

# Clone your plugin git repositories:
ARG PLUGIN_MODULE=github.com/trinnylondon/traefik-add-trace-id
ARG PLUGIN_GIT_REPO=https://github.com/trinnylondon/traefik-add-trace-id.git
ARG PLUGIN_GIT_BRANCH=v0.1.5
RUN --mount=type=git clone \
    --depth 1 --single-branch --branch ${PLUGIN_GIT_BRANCH} \
    ${PLUGIN_GIT_REPO} /plugins-local/src/${PLUGIN_MODULE}

FROM traefik:v2.10.3
COPY --from=0 /plugins-local /plugins-local
